#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source ~/aliases
source ~/funktion
source ~/git-prompt.sh
source ~/git-completion.bash
export GIT_PS1_SHOWDIRTYSTATE=1


#Webseite https://www.nerdfonts.com/cheat-sheet
#Crusor Pfeile
#nf-cod-chevron_right  nf-md-chevron_right

PS1='\e[0;34m \e[m in \w\e[0;31m$(__git_ps1 " (%s)")\e[m 󰅂 '




#PS1='\w$(__git_ps1 " (%s)")\$ '
export VISUAL=nano
export EDITOR=$VISUAL

