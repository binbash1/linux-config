
# Damit Arch Linux ein auf UTF-8 basierendes System mit deutschem
# Tastaturlayout erhält.

echo LANG=de_DE.UTF-8 > /etc/locale.conf
echo KEYMAP=de-latin1-nodeadkeys > /etc/vconsole.conf
ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# Die Hardwareuhr sollte auf die UTC Zeit einstellen

timedatectl set-local-rtc 0

# Setzen der LC-Variablen

export LC_DATE=de_DE.utf8  >> /etc/locale.conf
export LC_NUMERIC=de_DE.utf8 >> /etc/locale.conf
export LC_TIME=de_DE.utf8 >> /etc/locale.conf
export LANG=de_DE.utf8 >> /etc/locale.conf




